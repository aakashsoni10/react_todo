import React, {useState, useRef, useEffect} from 'react'
import TodoList from '../TodoList/TodoList';
import {AiFillPlusCircle} from 'react-icons/ai'
import './Home.css'
import Instruction from '../Instruction/Instruction';

const LOCALSTORAGE_KEY = 'todos_id';


//Getting Items from localstorage
const getLocalItems = () => {
  let list = localStorage.getItem(LOCALSTORAGE_KEY);

  if(list){
    return JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY));
  } else {
    return [];
  }
}

function User() {

    //Data State and Toggle State
    const [inputData, setInputData] = useState("");
    const [todos, setTodos] = useState(getLocalItems());
    const [isEditItem, setIsEditItem] = useState("");
    const [toggleButton, setToggleButton] = useState(false);

    //Time State
    const [timeStatus, setTimeStatus] = useState(true);
    const [time, setTime] = useState();

    //UseRef Function
    const todoRef = useRef();

    function Handler(e) {
      if(e){
        setInputData(e.target.value)
      }
      
    }
    //add Todo function
    const addTodo = (e) => {
        if(!inputData && inputData.length < 6 ) {
          alert('Fill Data to add Todo of min 6 length')
          return;
        } 
        if(inputData && !toggleButton){
           const elem =  todos.map((elem) => {
                if(elem.id === isEditItem){
                  return { text: inputData}
                }
                return elem;
              })
            setTodos(elem);
            setIsEditItem("");
            setInputData("");
        } 
        if(inputData && isEditItem === "") {
          const data = inputData.trim().split(/ +/).join(' ');  
          const allNewTodo ={id: Math.floor(Math.random() * 1000), text: data}
          if(allNewTodo.text !== ""){
              setTodos([...todos,allNewTodo]);
            } else {
              alert('Enter Todo Plzzz');
            }
            setInputData(" ")
        }
    }

    //delete function
    const deleteItem = (id) => {
       setTodos((items) => {
         return items.filter((ele, index) => {
          return index !== id; 
         })
       })
    }

    //edit function
    const editItem = (id) => {
      let findElement = todos.find((elem) => {
          return elem.id === id;
      })
      setInputData(findElement.text);
      setIsEditItem(id);
    }

     //Time Function
     function getTime(){
      var latTime = new Date().toLocaleTimeString();
        setTime(latTime)
    }

    setInterval(getTime, 1000);

    //set todo in Localstorage 
    useEffect(() => {
      localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(todos))
    }, [todos])


   


  return (
    <div className='main-div'>
          <Instruction/>
              {
                timeStatus ? <h1 className="time-div">{time}</h1> : null
              }
            <button className="time-button" onClick={() => {setTimeStatus(!timeStatus)}}>Show Time </button>
        <div className="center-div">
              <h1 className="header">ToDo List</h1>
              <input className='inputField' ref={todoRef} type="text" value={inputData} placeholder='Add a Item' onChange={(e) => {Handler(e)}}></input>
              <AiFillPlusCircle className='addButton' onClick={addTodo}/>
              <TodoList todos={todos} deleteItem ={deleteItem} editItem={editItem} setToggle={toggleButton}/>
        </div>
        <h4 className='title'>Aakash <span>{'@'}2022</span></h4>
    </div>
    
  )
}

export default User