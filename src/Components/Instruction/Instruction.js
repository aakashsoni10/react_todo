import React from 'react'
import './Instruction.css'

const segment = (props) => {
    return (
        <ul>
        <li className='list'>
                {props.children}
        </li>
        </ul>
    )
}

const Instruction = () => {
  return (
    <div>
        <div className='main-intro'>
            <h2>Instructions</h2>
                <segment>
                    <p>+ to Add todo</p>
                </segment>
                <segment>
                    <p>X to delete Todo</p>
                </segment>
                <segment>
                    <p>🖊 to edit Todo</p>
                </segment>            
        </div>
    </div>
  )
}



export default Instruction;
