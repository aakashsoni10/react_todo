import React, { useState } from 'react';
import {FaPenSquare} from 'react-icons/fa';
import {AiFillSave} from 'react-icons/ai';
import './Todo.css';

// const LOCALSTORAGE_KEY = 'todos_id';
export default function Todo({index,delId,id, todo, onSelect, onSelectEdit, setToggle}) {

  const [toggleButton , setToggleButton] = useState(setToggle);

  return (
    <div className="todoList">
        <ul>
          <button  className='delete' onClick={
             () => {
               onSelect(delId);
               }}>X</button>
            <li key={index}>
                {todo.text}
            </li>
            {
              toggleButton ? <AiFillSave className='edit-Button'/> : <FaPenSquare className='edit-Button' onClick={() => {onSelectEdit(id)}}/>
            }
            
        </ul>
    </div>
  )
}
