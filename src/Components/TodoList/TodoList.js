import React from 'react'
import Todo from '../Todo/Todo'
export default function TodoList({todos, deleteItem, editItem,setToggle}) {

  return (
    todos.map((todo, index) => {
        return( 
        <Todo 
            key={index}
            delId = {index}
            id={todo.id}
            todo={todo}
            onSelect={deleteItem}
            onSelectEdit = {editItem}
            setToggle = {setToggle}
          />
        )
    })
  )
}
